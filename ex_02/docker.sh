#!/bin/bash

# seus comandos aqui ...

docker volume create nginx_data

docker run -d -p 8001:80 --name nginx_01 -v nginx_data:/usr/share/nginx/html nginx:latest
docker run -d -p 8002:80 --name nginx_02 -v nginx_data:/usr/share/nginx/html nginx:latest

docker run -it -v nginx_data:/app debian-with-vim /usr/bin/vim /app/index.html
